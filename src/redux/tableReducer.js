

const tableReducer = (state = [],action)=>{
    console.log("Table reducer")
  
    switch (action.type){
        case 'FETCH_DATA':
          return  action.payload;
        default:
            return state
    }

}
export default tableReducer;


// const initialState = {
//     tableData :[]
// }

// const tableReducer = (state = initialState,action)=>{
//     console.log("Table reducer")
//     const newState ={...state}
//     switch (action.type){
//         case 'FETCH_DATA':
//           return  newState.tableData = action.payload;
//         default:
//             return state
//     }

// }
// export default tableReducer;