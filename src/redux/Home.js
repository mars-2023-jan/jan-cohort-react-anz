import React from 'react';
import { useSelector } from 'react-redux';
import { increment, decrement} from './actions';
import { useDispatch } from 'react-redux';
import { getData } from './actions';
import { Fragment } from 'react';

function Home(props) {
    console.log('State is getting accessed')
    const state = useSelector(state=>state)
    const dispatch = useDispatch()
    console.log("in Home.js"+state.tabledata)
    return (
        <div className='App'>
            <h2>counter :{state.count.counter}</h2>
            <button onClick={() => dispatch(increment())}>Add</button>
            <button onClick={() => dispatch(decrement())}>Subtract</button>
            <hr/>
            <input 
                type="radio"
                name="color"
                value="Red"
                onClick={(e)=>dispatch({type:'CHANGECOLOR',payload:e.target.value})}
                />RED&nbsp;
            <input 
                type="radio"
                name="color"
                value="Blue"
                onClick={(e)=>dispatch({type:'CHANGECOLOR',payload:e.target.value})}
                />BLUE&nbsp;
            <input 
                type="radio"
                name="color"
                value="Green"
                onClick={(e)=>dispatch({type:'CHANGECOLOR',payload:e.target.value})}
                />GREEN&nbsp;
            <p>My favorite color is : {state.color.color}</p>
            
            <hr/>
            <button onClick={()=>dispatch(getData())}>Get data</button>
            <Fragment>
                
                <table>
                <thead>
                    <tr>
                        <th scope="col">User Id</th>
                        <th scope="col">Id</th>
                        <th scope="col">Title</th>
                        <th scope="col">Body</th>
                    </tr>
                </thead>
                <tbody>
                {state.tabledata.map((item, index) => (
                    item.id % 2 === 0 ? 
                    <tr key={index}>
                        <td>{item.userId}</td>
                        <td>{item.id}</td>
                        <td>{item.title}</td>
                        <td>{item.body}</td>
                    </tr>
                    :
                    <tr>
                        <td>No data</td>
                       
                    </tr>
                ))}
                </tbody>
                </table>
            </Fragment>
        </div>
    );
}

export default Home;
