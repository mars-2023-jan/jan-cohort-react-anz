import { combineReducers } from "redux";
import colorReducer from "./colorReducer";
import counterReducer from "./counterReducer";
import tableReducer from "./tableReducer";

export const rootReducer = combineReducers({
    count:counterReducer,
    color:colorReducer,
    tabledata:tableReducer
})