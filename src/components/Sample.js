
import * as React from 'react';

const Sample = () => {
  const [checked, setChecked] = React.useState(false);
  const [checkedTwo, setCheckedTwo] = React.useState(false);
  const [checkedThree, setcheckedThree] = React.useState(false);
  const [checkedFour, setcheckedFour] = React.useState(false);


  const handleChange = (e) => {
    setChecked(!checked);
    let isChecked = e.target.checked;
    console.log(isChecked)
  };
  const handleChangeTwo = () => {
    setCheckedTwo(!checkedTwo);
  };
  const handleChangeThree = () => {
    setcheckedThree(!checkedThree);
  };
  const handleChangeFour = () => {
    setcheckedFour(!checkedFour);
  };
  return (
    <div>
      <Checkbox
        label="Sports"
        value={checked}
        onChange={handleChange}
      />
         <Checkbox
        label="Travel"
        value={checkedTwo}
        onChange={handleChangeTwo}
      />
        <Checkbox
        label="Music"
        value={checkedThree}
        onChange={handleChangeThree}
      />
        <Checkbox
        label="Readiing"
        value={checkedFour}
        onChange={handleChangeFour}
      />
      <p>My hobbies include : {checked.toString()}</p>
    </div>
  );
};
const Checkbox = ({ label, value, onChange }) => {
  return (
    <label>
      <input type="checkbox" checked={value} onChange={onChange} />
      {label}
    </label>
  );
};

export default Sample;