import React, { useState } from "react";

  
function CheckBox() {
  const [userinfo, setUserInfo] = useState({
    hobbies: [],
    response: [],
  });
  
  const handleChange = (e) => {
    // Destructuring
    const { value, checked } = e.target;
    const { hobbies } = userinfo;
      
    console.log(`${value} is ${checked}`);
     
    // Case 1 : The user checks the box
    if (checked) {
      setUserInfo({
        hobbies: [...hobbies, value],
        response: [...hobbies, value],
      });
    }
  
    // Case 2  : The user unchecks the box
    else {
      setUserInfo({
        hobbies: hobbies.filter((e) => e !== value),
        response: hobbies.filter((e) => e !== value),
      });
    }
  };
    
  return (
    <>
      <div className="container-fluid top ">
        <div className="container mt-5  pb-5 pt-5">
          {/* <h3 className="form-head-contact-h3 ">
            My hobbies include : {userinfo.response}
          </h3> */}
  
          <form>
          <div className="form-floating mt-3 mb-3 text-center">
            <label>My hobbies include :</label>
              <textarea
                className="form-control text"
                name="response"
                value= {userinfo.response}
                id="floatingTextarea2"
                style={{ width: "250px"}}
                onChange={handleChange}
              ></textarea>
            </div><br></br>
            <div className="row">
              <div className="col-md-6">
                <div className="form-check m-3">
                  <input
                    className="form-check-input"
                    type="checkbox"
                    name="hobbies"
                    value="Sports"
                    id="flexCheckDefault"
                    onChange={handleChange}
                  />
                  <label
                    className="form-check-label"
                    // htmlFor="flexCheckDefault"
                  >
                      Sports
                  </label>               
                  <input
                    className="form-check-input"
                    type="checkbox"
                    name="hobbies"
                    value="Music"
                    id="flexCheckDefault"
                    onChange={handleChange}
                  />
                  <label
                    className="form-check-label"
                    // htmlFor="flexCheckDefault"
                  >
                      Music
                  </label>
               
                  <input
                    className="form-check-input"
                    type="checkbox"
                    name="hobbies"
                    value="Travel"
                    id="flexCheckDefault"
                    onChange={handleChange}
                  />
                  <label
                    className="form-check-label"
                    // htmlFor="flexCheckDefault"
                  >
                      Travel
                  </label>
                
                  <input
                    className="form-check-input"
                    type="checkbox"
                    name="hobbies"
                    value="Reading"
                    id="flexCheckDefault"
                    onChange={handleChange}
                  />
                  <label
                    className="form-check-label"
                    // htmlFor="flexCheckDefault"
                  >
                      Reading
                  </label>
                </div>
              </div>   
            </div>
        
          </form>
        </div>
      </div>
    </>
  );
}
  
export default CheckBox;