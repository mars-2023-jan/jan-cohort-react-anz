import React, { useState } from "react";
import { useNavigate } from 'react-router-dom';

function Login() {

    const [email, setEmail] = useState("");
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const navigate = useNavigate();


    const validateForm=()=>{
        if(username === "ancy" && password === "123" && email === "a@a.b"){
            console.log(username)
            console.log(password)
            console.log(email)
            console.log("Login success")
            navigate('/loginsuccess')
        }else{
            console.log(username)
            console.log(password)
            console.log(email)
            console.log("Login failed")
            navigate('/loginfailed')
        }
    }


  return (

    <div className="Login">

      <form>
        <h1>Login Page</h1>
        <div>
            <label>Username : </label>
            <input type="text" value={username} onChange={(e)=> setUsername(e.target.value)} />
        </div>
        <div>
            <label>Password : </label>
        <input type="password" value={password} onChange={(e) => setPassword(e.target.value)}/>
        </div>
        <div>
            <label>Email : </label>
            <input type="email" value={email} onChange={(e) => setEmail(e.target.value)}/>
        </div>
      
        <div>
            <input type="submit"  value="Login" onClick={validateForm}/>
        </div>
        
      </form>

    </div>

  );

}
export default Login;
