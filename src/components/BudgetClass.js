import React, {Component} from "react";

class BudgetClass extends React.Component{
    constructor(){
        super();
        this.state = {
            budget: "",
            childcare:"",
            insurance:"",
            mortgage:"",
            grocery:"",
            balance:""
        };
        this.changeHandler = this.changeHandler.bind(this)
    }
    changeHandler = e => {
        this.setState({
            [e.target.name]: e.target.value 
        });
        
    }
    render(){
        return(
            <div id="budget_div">
                <form id="container-form">
                    <div>
                        <label>Total monthly budget : </label>
                        <input
                            type="number"
                            name="budget"
                            value={this.state.budget}
                            onChange={this.changeHandler}
                        />
                    </div>
                    <div>
                        <label>-----Expenses-----</label>
                    </div>
                    <div>
                        <label>Child care : </label>
                        <input
                            type="number"
                            name="childcare"
                            value={this.state.childcare}
                            onChange={this.changeHandler}
                        />
                    </div>
                    <div>
                        <label>Insurance : </label>
                        <input
                            type="number"
                            name="insurance"
                            value={this.state.insurance}
                            onChange={this.changeHandler}
                        />
                    </div>
                    <div>
                        <label>Mortgage : </label>
                        <input
                            type="number"
                            name="mortgage"
                            value={this.state.mortgage}
                            onChange={this.changeHandler}
                        />
                    </div>
                    <div>
                        <label>Grocery : </label>
                        <input
                            type="number"
                            name="grocery"
                            value={this.state.grocery}
                            onChange={this.changeHandler}
                        /> 
                    </div>
                    <div id="balance">Monthly balance : {(this.state.budget)-(this.state.childcare)-(this.state.insurance)-(this.state.mortgage)-(this.state.grocery)}</div>
                </form>
            </div>
        )
    }

}
export default BudgetClass;