import React from 'react';
import Budget from './Budget';
import BudgetClass from './BudgetClass';
import CheckBox from './CheckBox';
import Login from './Login';
import { BrowserRouter, Link, Routes, Route} from 'react-router-dom';
import Layout from './Layout';
import Home from './Home';
// import LoginFailed from './LoginFailed';
import Error from './Error';
import LoginFailed from './LoginFailed';
import LoginSuccess from './LoginSuccess';


function MyRouters(props) {
    return (
        <div>
            <BrowserRouter>
            <nav id='nav_id'>
                <ul>
                    <li><Link to="/budget">Budget using function</Link></li>
                    <li><Link to="/budgetclass">Budget using class</Link></li>
                    <li><Link to="/checkbox">Checkbox</Link></li>
                    <li><Link to="/layout">Layout</Link></li>
                    <li><Link to="/login">Login</Link></li>
                </ul>
            </nav>
            <Routes> 
                <Route path='/' element={<Home/>}/>           
                <Route path='/budget' element={<Budget/>}/>
                <Route path='/budgetclass' element={<BudgetClass/>}/>
                <Route path='/checkbox' element={<CheckBox/>}/>
                <Route path='/layout' element={<Layout/>}/>
                <Route path='/login' element={<Login/>} />  
                <Route path='*' element={<Error/>}/> 
                <Route path='/loginfailed' element={<LoginFailed/>}/>
                <Route path='/loginsuccess' element={<LoginSuccess/>}/>
            </Routes>
         </BrowserRouter>
        </div>
    );
}

export default MyRouters;