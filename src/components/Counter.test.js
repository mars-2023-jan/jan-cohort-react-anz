import React from "react";
import { fireEvent, render ,screen} from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import CounteT from "./CounteT";

describe('Testing',()=>{
    test('Counter  is incremented',()=>{
        render(<CounteT />)
        screen.debug();
        const counter = screen.getByTestId('counter')
  
        fireEvent.click(screen.getByText('Add'))
        fireEvent.click(screen.getByText('Add'))
        fireEvent.click(screen.getByText('Add'))
        fireEvent.click(screen.getByText('Add'))

        expect(counter.innerHTML).toBe("4")
    });
    test('Counter is decremented',()=>{
        render(<CounteT />)
        const counter = screen.getByTestId('counter')
   
        fireEvent.click(screen.getByText('Subtract'))
        fireEvent.click(screen.getByText('Subtract'))


        expect(counter.innerHTML).toBe("-2")
    });

})