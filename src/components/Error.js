import React from 'react';
import { useNavigate } from 'react-router-dom';

function Error(props) {

    const navigate = useNavigate();

    const homePage=()=>{
        console.log("Error page")
        navigate('/')
    }
    return (
        <div>
           
            <button onClick={homePage}>ERROR.Go back to home page!</button>
        </div>
    );
    }
export default Error;