import {useState} from 'react';

function Budget(){
    const [expense, setExpense] = useState({
        budget: "",
        childcare:"",
        insurance:"",
        mortgage:"",
        grocery:"",
        balance:""
    });

    const handleChange = (event) => {
        setExpense({ ...expense, [event.target.name]: event.target.value });
        console.log("handle change action")
        console.log(expense)
      };
  
    return (
        <div className="form-container" tabIndex="0" >
            <form >
                <div>
                    <label>Total Monthly Budget : </label>
                    <input
                    type="number"
                    name="budget"
                    value={expense.budget}
                    onChange={handleChange}
                    />
                </div>
                <div>
                    <label>----Expenses----</label>
                </div>
                <div>
                    <label>Child care : </label>
                    <input
                    type="number"
                    name='childcare'
                    value={expense.childcare}
                    onChange={handleChange}
                    />
                </div>
                <div>
                    <label>Insurance : </label>
                    <input
                    type="number"
                    name='insurance'
                    value={expense.insurance}
                    onChange={handleChange}
                    />
                </div>
                <div>
                    <label>Mortgage : </label>
                    <input
                    type='number'
                    name='mortgage'
                    value={expense.mortgage}
                    onChange={handleChange}
                    />
                </div>
                <div>
                    <label>Grocery : </label>
                    <input
                    type='number'
                    name='grocery'
                    value={expense.grocery}
                    onChange={handleChange}
                    
                    />
                </div>
                <div id="balance">Monthly balance : {(expense.budget)-(expense.childcare)-(expense.insurance)-(expense.mortgage)-(expense.grocery)}</div>
            </form>

        </div>
    );
}
export default Budget;
