import React from "react";

function Nav(){
    return(
        <div id="nav" >
            <div className="nav_items"><a href="#home">Home</a></div>
            <div className="nav_items"> <a href="#news">News</a></div>
            <div className="nav_items"><a href="#contact">Contact</a></div>
            <div className="nav_items"><a href="#about">About</a></div>

        </div>
    );
}
 export default Nav;