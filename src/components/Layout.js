import React from 'react';
import Header from './Header';
import Footer from './Footer';
import Mainpage from './Mainpage'
import Nav from './Nav';

function Layout(props) {
    return (
        <div>   
            <Header title="My header"/>
                <div id='wrapper'>
                <Nav />
                <Mainpage />
                </div>
            <Footer note="Footer Note"/>
            
        </div>
    );
}

export default Layout;