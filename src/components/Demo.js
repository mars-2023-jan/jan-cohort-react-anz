import React from 'react';

function Demo(props) {
    return (
        <div>
            <h1>Hello World</h1>
            <p data-testId='testId'></p>
            <ul>
                <li>A</li>
                <li>B</li>
                <li>C</li>
                <li>D</li>
            </ul>
        </div>
    );
}

export default Demo;