import React, {useEffect, useState} from 'react';
import Employees from './Employees';
import { useNavigate } from 'react-router-dom';

function EditEmployee(props) {
    const[name,setName]=useState('')
    const[id,setId]= useState('')
    const[age, setAge]= useState('')

    let navigate = useNavigate();

    let index = Employees.findIndex(val=>val.id==id)

    const handleClick = (e)=>{
        e.preventDefault();
        console.log("Editing")
        console.log(index)
        let emp = Employees[index];
        emp.name = name;
        emp.age = age;
        navigate('/')
        
    }


    useEffect(()=>{
        setName(localStorage.getItem('name'))
        setAge(localStorage.getItem('age'))
        setId(localStorage.getItem('id'))
    },[])
    

    return (
        <div>
            <form>
                <input
                    type='text'
                    placeholder='Enter Name'
                    value={name}
                    onChange={(e)=>setName(e.target.value)}
                />
                <br/><br/>
                <input
                    type='number'
                    placeholder='Enter age'
                    value={age}
                    onChange={(e)=>setAge(e.target.value)}
                />
                <br/><br/>
                <button onClick={(e)=>handleClick(e)} type="submit">Update</button>


        </form>   
        </div>
    );
}

export default EditEmployee;