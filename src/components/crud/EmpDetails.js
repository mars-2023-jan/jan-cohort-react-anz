import React, { Fragment, useState } from 'react';
import Employees from './Employees';
import { useNavigate, Link } from 'react-router-dom';
import './../crud/empDetails.css'
import {nanoid} from 'nanoid'
import ReadOnlyRow from './ReadOnlyRow';
import EditableRow from './EditableRow';

function EmpDetails(props) {

    let navigate = useNavigate();

    const[employees, setEmployees] = useState(Employees)

    const[editEmplyeeid, seteditEmplyeeid] =useState(null)

    const [addFormData, setaddFormData] =useState({
        name:'',
        age:''
    })

    const [editEmpData, setEditEmpData] = useState({
        name:'',
        age:''
    })

    const handleAddFormChange = (e)=>{
        e.preventDefault();
        const fieldName = e.target.getAttribute('name')
        const fieldValue = e.target.value

        const newFormData ={...addFormData}
        newFormData[fieldName] = fieldValue
        setaddFormData(newFormData)
        // console.log(addFormData)
    }
    const handleAddFormSubmit = (e)=>{
        e.preventDefault();

        const newEmp ={
            id:nanoid(),
            name:addFormData.name,
            age:addFormData.age
        }

        const newEmployees = [...employees, newEmp]
        setEmployees(newEmployees)
    }
    const handleEditClick = (e,emp)=>{
            e.preventDefault();
            seteditEmplyeeid(emp.id)

            const formValues ={
                name:emp.name,
                age:emp.age
            }

  

    }

    const handleEditChange = (e)=>{
        e.preventDefault();
        const fieldName = e.target.getAttribute('name')
        const fieldValue = e.target.value

        const newFormData = {...editEmpData}
        newFormData[fieldName] = fieldValue
        setEditEmpData(newFormData)
    }

    const handleEditEmpSubmit=(e)=>{
        e.preventDefault();
        const editedEmp = {
            id:editEmplyeeid,
            name:editEmpData.name,
            age:editEmpData.age
        };
        const newEmp = [...employees]
        const index = employees.findIndex((emp)=>emp.id=== editEmplyeeid)
        newEmp[index] = editedEmp;
        setEmployees(newEmp);
        seteditEmplyeeid(null)

    }
    const handleDelete = (e, empRow) => {
        e.preventDefault();
        const newEmp = [...employees];
        const index = employees.findIndex((emp) => emp.id === empRow.id);
        newEmp.splice(index, 1);
        setEmployees(newEmp);
      };
    
    // const handleDelete = (id)=>{
    //     let index = Employees.map(e=>e.id).indexOf(id);
    //     //console.log(index)
    //     Employees.splice(index,1)
    //     navigate('/')
    // }

    // const handleEdit = (id,name,age)=>{
    //     //console.log(id,name,age)
    //     localStorage.setItem('id',id)
    //     localStorage.setItem('name',name)
    //     localStorage.setItem('age',age)
    // }
    // const handleAdd =()=>{
    //     var EmpIds = Employees.map(elements => {
    //         return elements.id;
    //         });
    //     var maximum = Math.max(...EmpIds);
    //     console.log(maximum)
    //     if(Employees.length === 0){
    //         console.log("Employee array is empty")
    //         maximum=Math.floor(Math.random() * 10);
    //     }
    //     //alert(maximum)
    //     localStorage.setItem('maxId',maximum)
    // }


    return (
        <div>
            <form onSubmit={handleEditEmpSubmit}>
            <table>
                <thead>
                    <tr>
                        <th>NAME</th>
                        <th>AGE</th>
                        <th colSpan={2}>ACTION</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        employees.length>0
                        ?
                        employees.map((emp)=>{
                            return(
                                <Fragment>{
                                    editEmplyeeid === emp.id?(
                                   
                                    <EditableRow editEmpData={editEmpData}
                                    handleEditChange={handleEditChange}/>
                                    )
                                    :
                                   (
                                    <ReadOnlyRow emp = {emp}
                                    handleEditClick ={handleEditClick}
                                    handleDelete={handleDelete}/>
                                    )}
                                </Fragment>
                                
                            //   <tr key={emp.id}>
                            //         <td>{emp.name}</td>
                            //         <td>{emp.age}</td>
                            //         <td>
                            //             <Link to='/edit'>
                            //                 <button onClick={()=>handleEdit(emp.id,emp.name,emp.age)}>Edit</button>
                            //             </Link>
                            //         </td>
                            //         <td>
                            //             <button onClick={()=>handleDelete(emp.id)}>Delete</button>
                            //         </td>
                            //   </tr>

                        )
                        
                    })
                    :
                    'No data found'
                }
                </tbody>
            </table>
            </form>
            <br/><br/><br/>
            {/* <Link to='/add'>
                <button onClick={()=>handleAdd()}>Add employee</button>
            </Link> */}
            <h2>Add Employee</h2>
            <form onSubmit={handleAddFormSubmit}>
                <input
                    type="text"
                    name="name"
                    required="required"
                    placeholder='Enter Name'
                    onChange={handleAddFormChange}
                />

                <input
                    type="number"
                    name="age"
                    required="required"
                    placeholder='Enter Age'
                    onChange={handleAddFormChange}
                />
                <button type='submit'>Add employee</button>
            </form>
        </div>
    );
}

export default EmpDetails;