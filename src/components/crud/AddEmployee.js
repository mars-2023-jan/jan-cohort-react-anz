import { useEffect, useState } from "react";
import React from 'react';
import Employees from "./Employees";
import { useNavigate } from "react-router-dom";

function AddEmployee(props) {
    const [name, setName] = useState('')
    const [age, setAge] = useState('')
    const [maxid, setMaxId] = useState('')

    let navigate = useNavigate();


    const handleAdd = (e)=>{
   
        e.preventDefault()
        var maxId= maxid
        setMaxId(maxId++)
        console.log("Adding")
        console.log(maxId, age, name)
        const obj = {'id':maxId, 'name':name, 'age':age};     
        Employees.push(obj)
        navigate('/')

    }
    useEffect(()=>{
        setMaxId(localStorage.getItem('maxId'))
    },[])
    return (
        <div>
            <form>
                <div>
                    <label>Name : </label>
                    <input
                    type ='text'
                    placeholder="Enter the name"
                    onChange={(e)=>setName(e.target.value)}
                    />
                </div>
                <div>
                    <label>Age : </label>
                    <input
                    type='number'
                    placeholder="Enter the age"
                    onChange={(e)=>setAge(parseInt(e.target.value))}
                    />
                </div>
                <div>
                   <button onClick={(e)=>handleAdd(e)} type="submit">Submit</button>
                </div>
            </form>
        </div>
    );
}

export default AddEmployee;