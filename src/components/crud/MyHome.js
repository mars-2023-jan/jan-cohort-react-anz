import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import EmpDetails from './EmpDetails';
import EditEmployee from './EditEmployee';
import AddEmployee from './AddEmployee';
function MyHome(props) {
    return (
        <div>
            <Router>
                <Routes>
                     <Route path = '/' element={<EmpDetails/>}/>
                     <Route path='/edit' element={<EditEmployee/>}/>
                     <Route path='/add' element={<AddEmployee/>}/>
                </Routes>
                
            </Router>
        </div>
    );
}

export default MyHome;