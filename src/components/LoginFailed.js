import React from 'react';
//import { useNavigate } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';

function LoginFailed(props) {
    const navigate = useNavigate();

    const homePage=()=>{
        console.log("Login failed page")
        navigate('/')
    }
    return (
        <div>
        
            <button onClick={homePage}>Login failed. Go back to home page!</button>
        </div>
    );
}

export default LoginFailed;