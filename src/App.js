import logo from './logo.svg';
import './App.css';
import MyRouters from './components/MyRouters';
import MyHome from './components/crud/MyHome';
import Greet from './components/Greet';
import Welcome from './components/Welcome';
import Message from './components/Message';
import Counter from './components/Counter';
import { configureStore } from '@reduxjs/toolkit';
import Home from './redux/Home';
import myLogger from './redux/myLogger';
import counterReducer from './redux/counterReducer'
import capAtTen from './redux/capAtTen';
import { Provider} from 'react-redux'
import { rootReducer } from './redux/rootReducer';
import { composeWithDevTools } from 'redux-devtools-extension';
import CounteT from './components/CounteT';
import thunk from 'redux-thunk';


const reduxDevTools= composeWithDevTools();
function App() {
  console.log('CREATING STORE')
  const store = configureStore({reducer:rootReducer, middleware:[myLogger, capAtTen,thunk], devTools:reduxDevTools})
  return (
    <Provider store = {store}>
      <Home/>
    </Provider>

 
  );

  //  <div className="App">
         
  //         <CounteT/>
  //   </div>  
  // );
}

export default App;




   // <div className="App">
         
    //       <MyHome/>
    // </div>  
   {/* <Message/> */}
     {/* <Counter/> */}

    {/* <Greet/> */}

{/* <Greet name="John" place="Milwaukee">
<p>This is children props</p>
</Greet>
<Greet name="Mark" place="Brookfield">
<button>Action</button>
</Greet>
<Greet name="Matt" place="Waukesha"/>



<Welcome name="John" place="Milwaukee"/>
<Welcome  name="Mark" place="Brookfield"/>
<Welcome name="Matt" place="Waukesha"/> */}














   {/* <MyRouters/> */}
         {/* <MyHome/> */}
 {/* <Check_Box/> */}
          {/* <Login/> */}
      
      {/* <h1>Welcome to React App</h1>
      <Header title="My header"/>
    <div id='wrapper'>
      <Nav />
      <Mainpage />
    </div>
      <Footer note="Footer Note"/>
    <br></br>
    <h1>------------Budget using function--------------</h1>
    <Budget/>
    <h1>------------Budget using Class--------------</h1>
    <BudgetClass/> */}
  
      

      {/* <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header> */}